<?php
/*
Plugin Name: Finn Search
Plugin URI:
Description: The search resources allows you to search for classified ads much similar to www.finn.no. <br> Use shortcode <strong>[finn-search]</strong> in content editor or <strong>do_shortcode('[finn-search]')</strong> in category template. Use shortcode <strong>[finn-bolig]</strong> in content editor or <strong>do_shortcode('[finn-bolig]')</strong> in ad template.
Author: 
Author URI: 
Text Domain: finn-search
Domain Path: /languages/
Version: 1.0.0
*/

define('FINN_SEARCH_DIR', plugin_dir_path(__FILE__));
define('FINN_SEARCH_URL', plugins_url( plugin_basename(FINN_SEARCH_DIR), dirname(__FILE__) ) );

define('FINN_SEARCH_PLUGIN', __FILE__ );
define('FINN_SEARCH_PLUGIN_BASENAME', plugin_basename( FINN_SEARCH_PLUGIN ) );
define('FINN_SEARCH_PLUGIN_NAME', trim( dirname( FINN_SEARCH_PLUGIN_BASENAME ), '/' ) );

define('FINN_SEARCH_DIR_INCLUDES', admin_url( 'admin.php?page=finn-search/includes', 'http' ));

function finn_search_load(){
    if(is_admin())
        require_once(FINN_SEARCH_DIR.'includes/admin/admin.php');

    require_once(FINN_SEARCH_DIR.'includes/core.php');
}
finn_search_load();

register_activation_hook(__FILE__, 'finn_search_activation');
function finn_search_activation() {
    global $wpdb;

    $finnSearch = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_name = 'finn-search'");
    if (empty($finnSearch)){
        $wpdb->insert(
            'wp_posts',
            array(
                'ping_status' => 'publish',
                'post_author' => 1,
                'post_date' => date("Y-m-d H:I:s"),
                'post_type' => 'page',
                'post_title' => 'finn search',
                'post_name' => 'finn-search',
                'post_content' => '[finn-search]',
            )
        );
    }

    $finnBolig = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_name='finn-bolig'");
    if (empty($finnBolig)){
        $wpdb->insert(
            'wp_posts',
            array(
                'ping_status' => 'publish',
                'post_author' => 1,
                'post_date' => date("Y-m-d H:I:s"),
                'post_type' => 'page',
                'post_title' => 'finn bolig',
                'post_name' => 'finn-bolig',
                'post_content' => '[finn-bolig]',
            )
        );
    }

    $finnMyWishlist = $wpdb->get_results("SELECT ID FROM wp_posts WHERE post_name='finn-wishlist'");
    if (empty($finnMyWishlist)){
        $wpdb->insert(
            'wp_posts',
            array(
                'ping_status' => 'publish',
                'post_author' => 1,
                'post_date' => date("Y-m-d H:I:s"),
                'post_type' => 'page',
                'post_title' => 'finn My Wishlist',
                'post_name' => 'finn-wishlist',
                'post_content' => '[finn-wishlist]',
            )
        );
    }
}

register_uninstall_hook(__FILE__, 'finn_search_uninstall');
function finn_search_uninstall(){

}

require_once plugin_dir_path(__FILE__) . 'includes/fs-functions.php';

$cur_user_id = get_current_user_id();
