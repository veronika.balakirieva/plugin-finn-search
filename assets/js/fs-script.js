$(document).ready(function () {
    var cookieList = function(cookieName) {
        var cookie = $.cookie(cookieName);
        var items = cookie ? cookie.split(/,/) : new Array();
        return {
            "add": function(val) {
                items.push(val);
                $.cookie(cookieName, items.join(','), { path: '/' });
            },
            "remove": function (val) {
                //EDIT: Thx to Assef and luke for remove.
                var indx = items.indexOf(val);
                if(indx!=-1) items.splice(indx, 1);
                $.cookie(cookieName, items.join(','), { path: '/' });
            },
            "clear": function() {
                items = null;
                $.cookie(cookieName, null, { path: '/' });
            },
            "items": function() {
                return items;
            }
        }
    };

    var list = new cookieList("fslike");
    var arr = list.items();

    $('.fs-like').on('click', function () {
        var likeId = $(this).attr('data-like');

        if ( arr.indexOf( likeId ) != -1 ){
            list.remove(likeId);
            $(this).removeClass('active');
        } else {
            list.add(likeId);
            $(this).addClass('active');
        }
    });

    for (var key in arr) {
        $('#fs-like-'+arr[key]).addClass('active');
    }
//list.clear();

    //fsLikedsAddClass();

    $('.fs-select').niceSelect();

    /* open logined menu */
    $('.fs-login').on('click', function () {
        $(this).addClass('active');
    });
    /* close logined menu */
    $(document).mouseup(function (e) {
        var container = $(".fs-logined");
        if (container.has(e.target).length === 0){
            $('.fs-login').removeClass('active');
        }
    });

    $('.fs-filter-name').on('click', function () {
        var fs_filter_one = $(this).parent();
        fs_filter_one.toggleClass('hide-filters');
        if ( fs_filter_one.hasClass('hide-filters') ){
            fs_filter_one.find('.fs-list-filters').slideUp(250);
        } else {
            //fs_filter_one.find('.fs-filter-one').removeClass('dn');
            fs_filter_one.find('.fs-list-filters').slideDown(250);
        }
    });

    $('.sFilters').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        var fsFilters = $('.fs-content.fs-flex .fs-filters');
        fsFilters.slideDown(250);
        fsFilters.toggleClass('active');
        if (fsFilters.hasClass('active')){

        } else {
            fsFilters.slideUp(250);
        }
    });



    $('.menu__icon').on('click', function() {
        $(this).closest('.fs-nav').toggleClass('menu_state_open');
    });

});

/*function fsLikedsAddClass() {
    arr.forEach(function(items, i, arr) {
        var item = items.split('#');
        $('#fs-like-'+item[0]).addClass('active');
    });
}*/

//for (var key in arr) {
//    $('#fs-like-'+arr[key]).addClass('active');
//}



/*
 * fs-carousel
 * */
var step = 300;
var fsLength = 1;
var col = 1;
var scrollSpeed = 500;
function fsCarousel(direction){
    var wrapper = $('#fs-carousel-wrapper');
    var allLength = $('#fs-carousel-wrapper').width() - col * step;

    if ( fsLength < allLength && direction == 'next' ){
        direction = '-';
        fsLength = fsLength + step;
    }

    if ( fsLength != 0 && direction == 'prev' ){
        direction = '+';
        fsLength = fsLength - step;
    }

    if ( fsLength < allLength ){
        $('.fs-goRight').removeClass('fs-dNone');
    } else {
        $('.fs-goRight').addClass('fs-dNone');
    }

    if ( fsLength != 0 ){
        $('.fs-goLeft').removeClass('fs-dNone');
    } else {
        $('.fs-goLeft').addClass('fs-dNone');
    }

    wrapper.stop().animate({ 'margin-left': direction+'='+step },scrollSpeed);
}
function fsTabWidths(){
    var carouselWidth = $('#fs-carousel').width();

    /* количество елементов карусели */
    var colCarouselEl = $("#fs-carousel-wrapper li").length;

    var mr = 0;
    /*var mr = 90;
    if ( $(document).width() < 460 ){
        col = 1;
        mr = 0;
    } else if ( $(document).width() < 768 ){
        col = 2;
        mr = 30;
    } else if ( $(document).width() < 1000 ){
        col = 3;
        mr = 60;
    }*/

    /* задаем ширину одного едемента карусели */
    $('#fs-carousel-wrapper li').css('width', (carouselWidth-mr)/col);

    /* ширина карусели */
    var carouselWrapper = colCarouselEl*(carouselWidth-mr)/col+colCarouselEl * mr;
    $('#fs-carousel-wrapper').css('width', carouselWrapper);
    step = carouselWrapper / colCarouselEl;
}
$(window).on('ready load resize', fsTabWidths );
$(window).on('ready load resize', s_h_filters );

function s_h_filters() {
    $('#fs-carousel-container').css({'height' : $('#fs-carousel-container img').height()});

    var ww = $(window).width();
    if ( ww <= 768 ){
        $('.fs-filter-one').addClass('hide-filters');
        $('.fs-list-filters').css({'display':'none'});

        $('.fs-content.fs-flex .fs-filters-catalog').css({'display':'none'});
    } else {
        $('.fs-filter-one').removeClass('hide-filters');
        $('.fs-list-filters').css({'display':'block'});

        $('.fs-content.fs-flex .fs-filters-catalog').css({'display':'block'});
    }
}

/* validate number price input */
function fs_validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
    }
}