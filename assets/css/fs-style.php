<?php
/*** set the content type header ***/
header("Content-type: text/css");

$path = preg_replace('/wp-content.*$/','',__DIR__);
include($path.'wp-load.php');
$options = get_option('finn_search_options');

$textColor = $options['textColor'];
$borderColor = $options['borderColor'];
$priceColor = $options['priceColor'];

$pathToFonts = plugins_url('finn-search/includes/admin/assets/fonts/');

( $textColor != '' ) ? $textColor = $options['textColor'] : $textColor = '#2c3a4a';
( $borderColor != '' ) ? $borderColor = $options['borderColor'] : $borderColor = '#e8eef2';
( $priceColor != '' ) ? $priceColor = $options['priceColor'] : $priceColor = '#3dafff';
?>

:root {
    --text-color: <?php echo $textColor;?>;
    --border-color: <?php echo $borderColor;?>;
    --price-color: <?php echo $priceColor;?>;
    --font-family: SFUIText;
}


/* FONTS */
@font-face {
    font-family: 'SFUIText';
    src: url('<?php echo $pathToFonts;?>SFUIText/SFUIText-Regular.woff2') format('woff2'),
    url('<?php echo $pathToFonts;?>SFUIText/SFUIText-Regular.woff') format('woff');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: 'SFUIText';
    src: url('<?php echo $pathToFonts;?>SFUIText/SFUIText-Light.woff2') format('woff2'),
    url('<?php echo $pathToFonts;?>SFUIText/SFUIText-Light.woff') format('woff');
    font-weight: 300;
    font-style: normal;
}
@font-face {
    font-family: 'SF UI  Text';
    src: url('<?php echo $pathToFonts;?>SFUIText/SFUIText-Medium.woff2') format('woff2'),
    url('<?php echo $pathToFonts;?>SFUIText/SFUIText-Medium.woff') format('woff');
    font-weight: 500;
    font-style: normal;
}
@font-face {
    font-family: 'SFUIText';
    src: url('<?php echo $pathToFonts;?>SFUIText/SFUIText-Bold.woff2') format('woff2'),
    url('<?php echo $pathToFonts;?>SFUIText/SFUIText-Bold.woff') format('woff');
    font-weight: bold;
    font-style: normal;
}
/* FONTS END */

.fs-ad-content,
.fs-wrap{
    font-family: var(--font-family);
}

#fs_itemFilterForm button,
.fs-checkbox:not(checked) + label:before,
.fs-checkbox:not(checked) + label:after,
.nice-select,
.fs-unit-desc,
.fs-prices input,
.fs-contact-person-contacts .fs-phone,
#fs-find{
    border: 1px solid var(--border-color);
}
.fs-filter-one{
    border-bottom: 1px solid var(--border-color);
}
.fs-content{
    border-top: 1px solid var(--border-color);
}

.fs-contact-person-contacts .fs-phone,
.fs-info .fs-contact-person-name p,
.fs-info .fs-address,
.fs-info .fs-code p,
.fs-ad-content h2,
.fs-ad-content p,
.fs-h1,
button.fs-button-lg,
.fs-sort,
.fs-prices input,
.fs-filter-name,
.fs-list-group label,
#fs-find,
.fs-unit-name a,
.fs-unit-type div:last-child{
    color: var(--text-color);
}

.fs-search-returned span,
.fs-unit-price div:first-child{
    color: var(--price-color);
}

.fs-info{
    border-bottom: 1px solid var(--border-color);
}
.fs-search-returned span{
    border-bottom: 1px solid var(--price-color);
}

header.fs-header .fs-login:after,
.nice-select:after{
    border-bottom: 2px solid var(--price-color);
    border-right: 2px solid var(--price-color);
}

.fs-pics-count{
    color: var(--border-color);
}

/* HEADER */
    header.fs-header,
    footer.fs-footer{
        background-color: var(--text-color);
        font-family: var(--font-family);
    }
    .fs-socLinks a,
    .fs-menu a{
        color: var(--border-color);
    }
    header.fs-header .fs-logined a:hover,
    .fs-socLinks a:hover,
    .fs-menu a:hover{
        color: var(--price-color);
    }
    header.fs-header .fs-logined{
        border-top:2px solid var(--price-color);
    }
/* HEADER END */

/* FOOTER */
    .fs-footer{
        background-color: var(--text-color);
    }
/* FOOTER END */

<?php require_once "fs-style.css";