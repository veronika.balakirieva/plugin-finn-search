<?php
function debug($arr){
    echo '<pre>'. print_r($arr, true) .'</pre>';
}

add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );
add_action( 'admin_init', 'theme_name_scripts' );
function theme_name_scripts() {
    wp_enqueue_style( 'fs-style', FINN_SEARCH_URL.'/assets/css/fs-style.php');
    wp_enqueue_script('fs-jquery', FINN_SEARCH_URL . '/assets/js/jquery.js', array(), '1.4.1', true );
    wp_enqueue_script('jquery.cookie', FINN_SEARCH_URL . '/assets/js/jquery.cookie.js', array(), '1.4.1', true );
    wp_enqueue_script('nice-select', FINN_SEARCH_URL . '/assets/js/jquery.nice-select.min.js', array(), '1.0.0', true );
    wp_enqueue_script('fs-script', FINN_SEARCH_URL . '/assets/js/fs-script.js', array(), '1.0.0', true );
}

function fs_get_option($cell){
    $options = get_option('finn_search_options');
    if ( !empty($cell) )
        return $options[$cell];
}

function alert_error($alert){
    echo '<div class="alert alert-danger" role="alert">'.$alert.'</div>';
}

function finn_shortcode( $atts ){
    if ( !fs_get_option('api_key') ){
        alert_error('No Finn API key');
    }

    if ( !fs_get_option('orgId') ){
        alert_error('No Finn API org id');
    }

    if ( fs_get_option('api_key') && fs_get_option('orgId') ){
        $tpl = FINN_SEARCH_DIR.'/includes/boligsok.php';
        require $tpl;
    }
}
add_shortcode('finn-search', 'finn_shortcode' );

function finn_bolig_shortcode( $atts ){
    if ( !fs_get_option('api_key') ){
        alert_error('no Finn API key');
    }

    if ( fs_get_option('api_key') ){
        $tpl = FINN_SEARCH_DIR.'/includes/bolig.php';
        require $tpl;
    }
}
add_shortcode('finn-bolig', 'finn_bolig_shortcode' );


function finn_wishlist_shortcode( $atts ){
    if ( !fs_get_option('api_key') ){
        alert_error('no Finn API key');
    }

    if ( fs_get_option('api_key') ){
        $tpl = FINN_SEARCH_DIR.'/includes/wishlist.php';
        require $tpl;
    }
}
add_shortcode('finn-wishlist', 'finn_wishlist_shortcode' );