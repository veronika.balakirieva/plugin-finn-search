<?php
class Finn_Search {
    public function fsCurl($apiUrl){
        $apiKey = fs_get_option('api_key');
        $userAgent = fs_get_option('userAgent');

        //fetch using curl (or whatever you prefer, curl is good for setting custom headers)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_URL, utf8_decode($apiUrl));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("x-finn-apikey: $apiKey"));
        $rawData = curl_exec($ch);

        if (curl_error($ch)) {
            die("Fetch problem");
        }
        //parse the xml and get namespaces (needed later to extract attributes and values)
        $xmlData = new SimpleXMLElement($rawData);
        return $xmlData;
    }

    public function fs_filters($xmlData, $admin = false){
        $ns = $xmlData->getNamespaces(true);

        $optionFilters = get_option('finn_search_options');
        if ( empty($optionFilters['filter']) ) $optionFilters['filter'] = [];

        $filtersArray = array();
        foreach( $xmlData->children($ns['f'])->filter as $filter ){
            if (
                ($admin == false)
                    ?
                in_array($filter->attributes()->title, $optionFilters['filter'])
                    :
                $filter->attributes()->title
            ){
                $filtersArray[$i] = [
                    'filterName' => $filter->attributes()->name,
                    'filterTitle' => $filter->attributes()->title,
                    'attributes' => array(),
                ];
                foreach ($filter->children($ns['f'])->Query as $f) {
                    $filtersArray[$i]['attributes'][$j] = [
                        "title" => $f->attributes()->title,
                        "totalResults" => $f->attributes()->totalResults,
                        "value" => $f['filter'],
                        'filterName' => $filter->attributes()->name,
                    ];

                    foreach( $f->children($ns['f'])->filter as $region ){
                        foreach( $region->children($ns['f'])->Query as $region ){
                            $filtersArray[$i]['attributes'][$j]['region'][$h] = [
                                'city' => $region->attributes()->title,
                                'totalResults' => $region->attributes()->totalResults,
                                'value' => $region['filter'],
                            ];
                            $h++;
                        }
                    }
                    $j++;
                }
            }
            $i++;
        }

        return $filtersArray;
    }

    public function fs_wishlidt($xmlData, $fsLiked = false ){
        $ns = $xmlData->getNamespaces(true);

        //get each entry for simpler syntax when looping through them later
        $entries = array();
        foreach ($xmlData->entry as $entry) {
            array_push($entries, $entry);
        }
        $ads = array();
        foreach ($entries as $entry) {
            $id = $entry->children($ns['dc'])->identifier;
            if ( in_array($id, $fsLiked) ) {
                $title = $entry->title;
                $updated = $entry->updated;
                $published = $entry->published;
                $isPrivate = "false";
                $status = "";
                $adType = "";
                foreach ($entry->category as $category) {
                    if ($category->attributes()->scheme =="urn:finn:ad:private"){
                        $isPrivate = $category->attributes()->term;
                    }
                    //if disposed == true, show the label
                    if ($category->attributes()->scheme =="urn:finn:ad:disposed"){
                        if($entry->category->attributes()->term == "true"){
                            $status = $category->attributes()->label;
                        }
                    }
                    if ($category->attributes()->scheme =="urn:finn:ad:type"){
                        $adType = $category->attributes()->label;
                    }
                }

                $georss = $entry->children($ns['georss'])->point;
                $location = $entry->children($ns['finn'])->location;
                $city = $location->children($ns['finn'])->city;
                $address = $location->children($ns['finn'])->address;
                $postalCode = $location->children($ns['finn'])->{'postal-code'};
                unset($img);
                if ($entry->children($ns['media']) && $entry->children($ns['media'])->content->attributes()) {
                    $img = $entry->children($ns['media'])->content->attributes();
                }
                $author = $entry->author->name;
                $adata = $entry->children($ns['finn'])->adata;
                $livingSizeFrom = 0;
                $livingSizeTo = 0;
                $propertyType = "";
                $numberOfBedrooms = 0;
                $bra = 0;
                $ownershipType = "";
                $usableSize = "";
                $primarySize = "";
                foreach ($adata->children($ns['finn'])->field as $field) {
                    if ($field->attributes()->name == 'no_of_bedrooms') {
                        $numberOfBedrooms = $field->attributes()->value;
                    }
                    if ($field->attributes()->name == 'property_type') {
                        $propertyType = $field->children($ns['finn'])->value;
                    }
                    if ($field->attributes()->name == 'ownership_type') {
                        $ownershipType = $field->attributes()->value;
                    }
                    if ($field->attributes()->name == 'size') {
                        foreach ($field->children($ns['finn'])->field as $sizeField) {
                            if ($sizeField->attributes()->name == "usable") {
                                $usableSize = $sizeField->attributes()->value;
                            }
                            if ($sizeField->attributes()->name == "primary") {
                                $primarySize = $sizeField->attributes()->value;
                            }
                            if ($sizeField->attributes()->name == "living") {
                                $bra = $sizeField->attributes()->value;
                            }
                            $livingSizeFrom = $sizeField->attributes()->from;
                            $livingSizeTo = $sizeField->attributes()->to;
                        }
                    }
                }
                $mainPrice = "";
                $totalPrice = "";
                $collectiveDebt = "";
                $sharedCost = "";
                $estimatedValue = "";
                $sqmPrice = "";
                foreach ($adata->children($ns['finn'])->price as $price) {
                    if ($price->attributes()->name == 'main') {
                        $mainPrice = $price->attributes()->value;
                    }
                    if ($price->attributes()->name == 'total') {
                        $totalPrice = $price->attributes()->value;
                    }
                    if ($price->attributes()->name == 'collective_debt') {
                        $collectiveDebt = $price->attributes()->value;
                    }
                    if ($price->attributes()->name == 'shared_cost') {
                        $sharedCost = $price->attributes()->value;
                    }
                    if ($price->attributes()->name == 'estimated_value') {
                        $estimatedValue = $price->attributes()->value;
                    }
                    if ($price->attributes()->name == 'square_meter') {
                        $sqmPrice = $price->attributes()->value;
                    }
                }

                $ads[$k] = [
                    "id" => $id,
                    "title" => $title,
                    "address" => $address,
                    "city" => $city,
                    "mainPrice" => $mainPrice,
                    "image" => $img->url,
                    "bra" => $bra,
                    "propertyType" => $propertyType,
                ];
                $k++;
            }
        }

        return $ads;
    }

    public function fs_ads($xmlData){
        $ns = $xmlData->getNamespaces(true);

        //get each entry for simpler syntax when looping through them later
        $entries = array();
        foreach ($xmlData->entry as $entry) {
            array_push($entries, $entry);
        }
        $ads = array();
        foreach ($entries as $entry) {
            $id = $entry->children($ns['dc'])->identifier;
            $title = $entry->title;
            $updated = $entry->updated;
            $published = $entry->published;
            $isPrivate = "false";
            $status = "";
            $adType = "";
            foreach ($entry->category as $category) {
                if ($category->attributes()->scheme =="urn:finn:ad:private"){
                    $isPrivate = $category->attributes()->term;
                }
                //if disposed == true, show the label
                if ($category->attributes()->scheme =="urn:finn:ad:disposed"){
                    if($entry->category->attributes()->term == "true"){
                        $status = $category->attributes()->label;
                    }
                }
                if ($category->attributes()->scheme =="urn:finn:ad:type"){
                    $adType = $category->attributes()->label;
                }
            }

            $georss = $entry->children($ns['georss'])->point;
            $location = $entry->children($ns['finn'])->location;
            $city = $location->children($ns['finn'])->city;
            $address = $location->children($ns['finn'])->address;
            $postalCode = $location->children($ns['finn'])->{'postal-code'};
            unset($img);
            if ($entry->children($ns['media']) && $entry->children($ns['media'])->content->attributes()) {
                $img = $entry->children($ns['media'])->content->attributes();
            }
            $author = $entry->author->name;
            $adata = $entry->children($ns['finn'])->adata;
            $livingSizeFrom = 0;
            $livingSizeTo = 0;
            $propertyType = "";
            $numberOfBedrooms = 0;
            $bra = 0;
            $ownershipType = "";
            $usableSize = "";
            $primarySize = "";
            foreach ($adata->children($ns['finn'])->field as $field) {
                if ($field->attributes()->name == 'no_of_bedrooms') {
                    $numberOfBedrooms = $field->attributes()->value;
                }
                if ($field->attributes()->name == 'property_type') {
                    $propertyType = $field->children($ns['finn'])->value;
                }
                if ($field->attributes()->name == 'ownership_type') {
                    $ownershipType = $field->attributes()->value;
                }
                if ($field->attributes()->name == 'size') {
                    foreach ($field->children($ns['finn'])->field as $sizeField) {
                        if ($sizeField->attributes()->name == "usable") {
                            $usableSize = $sizeField->attributes()->value;
                        }
                        if ($sizeField->attributes()->name == "primary") {
                            $primarySize = $sizeField->attributes()->value;
                        }
                        if ($sizeField->attributes()->name == "living") {
                            $bra = $sizeField->attributes()->value;
                        }
                        $livingSizeFrom = $sizeField->attributes()->from;
                        $livingSizeTo = $sizeField->attributes()->to;
                    }
                }
            }
            $mainPrice = "";
            $totalPrice = "";
            $collectiveDebt = "";
            $sharedCost = "";
            $estimatedValue = "";
            $sqmPrice = "";
            foreach ($adata->children($ns['finn'])->price as $price) {
                if ($price->attributes()->name == 'main') {
                    $mainPrice = $price->attributes()->value;
                }
                if ($price->attributes()->name == 'total') {
                    $totalPrice = $price->attributes()->value;
                }
                if ($price->attributes()->name == 'collective_debt') {
                    $collectiveDebt = $price->attributes()->value;
                }
                if ($price->attributes()->name == 'shared_cost') {
                    $sharedCost = $price->attributes()->value;
                }
                if ($price->attributes()->name == 'estimated_value') {
                    $estimatedValue = $price->attributes()->value;
                }
                if ($price->attributes()->name == 'square_meter') {
                    $sqmPrice = $price->attributes()->value;
                }
            }

            $ads[$k] = [
                "id" => $id,
                "title" => $title,
                "address" => $address,
                "city" => $city,
                "mainPrice" => $mainPrice,
                "image" => $img->url,
                "bra" => $bra,
                "propertyType" => $propertyType,
            ];
            $k++;
        }

        return $ads;
    }

    public function fs_searchTotalResults($xmlData){
        $ns = $xmlData->getNamespaces(true);

        $searchTitle = $xmlData->title;
        $searchSubTitle = $xmlData->subtitle;
        $searchTotalResults = $xmlData->children($ns['os'])->totalResults;

        return $searchTotalResults;
    }

    public function fs_searchTitle($xmlData){
        $searchTitle = $xmlData->title;

        return $searchTitle;
    }

    public function fs_adId($xmlData){
        $ns = $xmlData->getNamespaces(true);

        $adId = $xmlData->children($ns['dc'])->identifier;

        return $adId;
    }

    public function fs_adUrl(){
        if ( !empty(fs_get_option('adUrl')) ){
            $adUrl = '/'.fs_get_option('adUrl');
        } else {
            $adUrl = '/finn-bolig';
        }
        return $adUrl;
    }

    public function fs_locations($xmlData){
        $ns = $xmlData->getNamespaces(true);

        $locationsArr = $xmlData->children($ns['finn'])->location;
        foreach( $locationsArr as $location ){
            foreach( $location->children($ns['finn']) as $key => $loc ){
                $name = $key;
                $value = $loc;
                $locations["$name"] = "$value";
            }
        }

        return $locations;
    }

    public function fs_geo($xmlData){
        $ns = $xmlData->getNamespaces(true);

        $georss = $xmlData->children($ns['georss'])->point;
        $georss = explode(' ', $georss);
        //$georssArr = explode(' ', $georss);
        //$lat = $georssArr[0];
        //$lng = $georssArr[1];

        return $georss;
    }

    public function fs_contacts($xmlData){
        $ns = $xmlData->getNamespaces(true);

        $contactsArr = $xmlData->children($ns['finn'])->contact;
        $contacts = array();
        foreach( $contactsArr as $key => $contact ){
            $tel = array();
            foreach( $contact as $phone ){
                $tel[] = [
                    "tel" => $phone,
                ];
            }
            $contacts[] = [
                "title" => $contact->attributes()->title,
                "role" => $contact->attributes()->role,
                "name" => $contact->children($ns['name'])->name,
                "email" => $contact->children($ns['name'])->email,
                "phone" => $tel,
            ];
        }

        return $contacts;
    }

    public function fs_finn_code($xmlData){
        $ns = $xmlData->getNamespaces(true);

        $finn_kode = $xmlData->children($ns['dc'])->identifier;

        return $finn_kode;
    }

    public function fs_prices($xmlData){
        $ns = $xmlData->getNamespaces(true);

        $adata = $xmlData->children($ns['finn'])->adata;
        foreach($adata as $data){
            $prices = array();
            foreach( $data->children($ns['finn'])->price as $price ){
                $name = $price->attributes()->name;
                $value = $price->attributes()->value;
                $prices["$name"] = "$value";
            }
        }

        return $prices;
    }

    public function fs_datas($xmlData){
        $ns = $xmlData->getNamespaces(true);
        $adata = $xmlData->children($ns['finn'])->adata;

        foreach($adata as $data){
            $datas = array();
            foreach( $data->children($ns['finn'])->field as $dat ){
                $name = $dat->attributes()->name;
                $value = $dat->attributes()->value;
                if ( $dat->attributes()->name == 'timespan' ) {
                    $value = $dat->attributes()->from." - ".$dat->attributes()->to;
                }
                $datas["$name"] = "$value";
                if ( $dat->attributes()->name == 'general_text' ){
                    foreach( $dat->value as $dat_value ){
                        foreach( $dat_value as $item) {
                            if ( $item->attributes()->name == 'value' ){
                                $ad_value = $item;
                            }
                        }
                    }
                }

                if ( $dat->attributes()->name == 'facilities' ){
                    foreach( $dat->value as $faciliti ){
                        $facilities .= $faciliti.', ';
                    }
                }

                if ( $dat->attributes()->name == 'size' ){
                    foreach( $dat->field as $dat_value ){
                        $name = $dat_value->attributes()->name;
                        $value = $dat_value->attributes()->value;
                        $sizes["$name"] = "$value";
                    }
                }

                if ( $dat->attributes()->name == 'property_type' ) {
                    foreach( $dat->value as $dat_value ){
                        $boligtype = $dat_value;
                    }
                }
            }

        }

        $dat = array();
        $dat = [
            'facilities' => $facilities,
            'sizes' => $sizes,
            'boligtype' => $boligtype,
            'ad_value' => $ad_value,
            'datas' => $datas,
        ];

        return $dat;
    }

    public function fs_medias($xmlData){
        $ns = $xmlData->getNamespaces(true);

        $mediasContent = $xmlData->children($ns['media'])->content;
        $medias = array();
        foreach( $mediasContent as $mediaContent ){
            $medias[] = [
                "url" => $mediaContent->attributes()->url,
                "image" => $mediaContent->attributes()->medium,
                "width" => $mediaContent->attributes()->width,
                "height" => $mediaContent->attributes()->height,
                "description" => $mediaContent->children($ns['media'])->description,
            ];
        }

        return $medias;
    }

    public function fs_pagination($xmlData){
        $ns = $xmlData->getNamespaces(true);

        $searchTotalResults = $xmlData->children($ns['os'])->totalResults;

        //navigation links
        $links = array();
        foreach ($xmlData->link as $link) {
            $rel = $link->attributes()->rel;
            $ref = $link->attributes()->href;
            $links["$rel"] = "$ref";
        }

        $page_self = $links['self'];
        $page_self = explode('page=', $page_self);
        $page_self = explode('&', $page_self[1]);
        empty($page_self[0]) ? $page_self = 1 : $page_self = $page_self[0];

        $page_prev = $links['prev'];
        $page_prev = explode('page=', $page_prev);
        $page_prev = $page_prev[1];
        if( empty($page_prev) && $page_self == 2 ) $page_prev = 1;

        $page_next = $links['next'];
        $page_next = explode('page=', $page_next);
        $page_next = $page_next[1];


        $adsOnPage = $_REQUEST['rows'];
        if( !isset($adsOnPage) ){
            $adsOnPage = 14;
        };

        if($searchTotalResults > $adsOnPage){
            $pages = ceil($searchTotalResults / $adsOnPage);

            $page = intval($page_self);
            $num = 1;
            if ($page==0) $page=1;
            $posts = $pages;
            $total = intval(($posts - 1) / $num) + 1;
            $page = intval($page);
            if(empty($page) or $page < 0) $page = 1;
            if($page > $total) $page = $total;
            $start = $page * $num - $num;

            if($page != 1) $pervpage = '<button name="self" value="'. ($page - 1).'">&laquo;</button>';
            if($page != $total) $nextpage = '<button name="self" value="'. ($page + 1).'">&raquo;</button>';
            if($page - 2 > 0) $page2left = '<button name="self" value="'. ($page - 2) .'">'. ($page - 2) .'</button>';
            if($page - 1 > 0) $page1left = '<button name="self" value="'. ($page - 1) .'">'. ($page - 1) .'</button>';
            if($page + 2 <= $total) $page2right = '<button name="self" value="'. ($page + 2).'">'. ($page + 2) .'</button>';
            if($page + 1 <= $total) $page1right = '<button name="self" value="'. ($page + 1).'">'. ($page + 1) .'</button>';

            if ($total>1) {
                $fs_pagination_ = $pervpage.$page2left.$page1left.'<button class="fs-active" name="self" value="#">'.$page.'</button>'.$page1right.$page2right.$nextpage;

                $fs_pagination = '
                <div class="fs-pagination">
                    '.$fs_pagination_.'
                </div>
                ';

                return $fs_pagination;
            }
        }
    }

    public function fs_utleid($xmlData){

        $utleid = false;
        foreach ($xmlData->category as $link) {
            $label = $link->attributes()->label;
            $term = $link->attributes()->term;
            $categorys["$label"] = "$label";
            if ( $label == 'Utleid' && $term == 'true'){
                $utleid = true;
            }
        }
    }
}
