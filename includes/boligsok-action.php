<?php
$path = preg_replace('/wp-content.*$/','',__DIR__);
include($path.'wp-load.php');

!empty($row) ? $rows = '&'.$row : '&rows=14';

$result = parse_url($_SERVER['REQUEST_URI']);

$par = parse_str($result['query'],$params);

$maxPages = ceil($params['searchTotalResults'] / $params['rows']);

if ($maxPages < $params['self']){
    unset($params['self']);
}

unset($params['searchTotalResults']);

$ps_front_page = $params['fs-page'];

if ( empty($params) ) {$rows='';}

if ( empty($params['reset']) ){
    unset($params['fs-page']);
    foreach( $params as $key => $param ){
        $val = '';
        if(is_array($param)) {
            $array_pop = array_pop($param);
            foreach($param as $par_val) {
                $val .= $par_val . ',';
            }
            $paramValues = $key . '=' . $val . $array_pop."&";
        } else {
            if ( $key == 'fs_find' ){
                if ( !empty($param) ){
                    $paramValues = 'sok' . '='.str_replace(' ', '-', $param).'&';
                } else {
                    $paramValues = '';
                }
            } else {
                $paramValues = $key . '=' . $param."&";
            }
        }
        $new_url .= $paramValues;
    }
    $new_url = $ps_front_page.'?'.substr($new_url, 0, -1);
} else {
    $new_url = $params['fs-page'];
}

wp_safe_redirect( $new_url );