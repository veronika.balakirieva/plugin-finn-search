<?php $ad_id = $_REQUEST['id'];
if (isset($ad_id) && !empty($ad_id)) {
$apiUrl = "https://cache.api.finn.no/iad/ad/realestate-letting/$ad_id";

$fs = new Finn_Search;
$ps = $fs::fsCurl($apiUrl);
$searchTitle = $fs::fs_searchTitle($ps);
$adId = $fs::fs_adId($ps);
$locations = $fs::fs_locations($ps);
$geo = $fs::fs_geo($ps);
$contacts = $fs::fs_contacts($ps);
$finn_kode = $fs::fs_finn_code($ps);
$datas = $fs::fs_datas($ps);
$prices = $fs::fs_prices($ps);
$medias = $fs::fs_medias($ps);
$utleid = $fs::fs_utleid($ps);
?>

    <div class="fs-wrap">
        <div class="fs-header">
            <div class="fs-h1"><?php echo $searchTitle;?></div>
            <div class="fs-sub-title">
                <span>posted:</span> 28.06.2018
            </div>
        </div>

        <div class="fs-content fs-content-case fs-flex">
            <div class="fs-filters">
                <div class="fs-info">
                    <p>KONTAKTPERSON</p>
                    <div class="fs-contact-person">
                        <?php foreach( $contacts as $contact ) { ?>
                            <div class="fs-contact-person-i-n fs-flex">
                                <?php if ( !empty($contact['phone']) && $contact['email'] ) {?>
                                    <div class="fs-contact-person-photo">
                                        <?php if ( !empty($contact['avatar']) ) { ?>
                                        <img width="50" src="<?php echo $contact['avatar']; ?>" alt="<?php echo $contact['name']; ?>">
                                        <?php } else { ?>
                                        <img width="50" src="<?php echo FINN_SEARCH_URL; ?>/assets/img/user.svg" alt="<?php echo $contact['name']; ?>">
                                        <?php }?>
                                    </div>
                                    <div class="fs-contact-person-name">
                                        <p class="fs-name"><?php echo $contact['name']; ?></p>
                                        <p><?php echo $contact['title']; ?></p>
                                    </div>
                                <?php } else { ?>
                                    <p class="fs-contact-person-name">
                                        <?php echo $contact['name']; ?> <?php echo $contact['title']; ?>
                                    </p>
                                <?php } ?>
                            </div>
                            <?php if ( !empty($contact['phone']) && $contact['email'] ) {?>
                            <div class="fs-contact-person-contacts fs-flex">
                                <?php if ( !empty($contact['phone']) ) { ?>
                                    <?php foreach( $contact['phone'] as $phone ){ ?>
                                    <a href="tel:<?php echo $phone['tel'];?>" class="fs-phone">
                                        <?php echo $phone['tel'];?>
                                    </a>
                                    <?php } ?>
                                <?php } ?>
                                <?php if ( !empty($contact['email']) ) {?>
                                    <a href="mailto:<?php echo $contact['email'];?>" class="button fs-active">
                                        Send e-post
                                    </a>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="fs-info">
                    <p><span>Depositum: </span><span><?php echo $prices['deposit'];?></span></p>
                    <p><span>Primærrom: </span><span><?php echo $datas['sizes']['primary']?> m²</span></p>
                    <p><span>Soverom: </span><span><?php echo $datas['datas']['no_of_bedrooms'];?></span></p>
                    <p><span>Etasje: </span><span><?php echo $datas['datas']['floor'];?></span></p>
                    <p><span>Boligtype: </span><span><?php echo $datas['boligtype'];?></span></p>
                    <p><span>Leieperiode: </span><span><?php echo $datas['datas']['timespan'];?></span></p>
                </div>
                <div class="fs-info">
                    <p>VISNING</p>
                    <div class="fs-code">
                        <p>FINN-kode: <?php echo $finn_kode;?></p>
                    </div>
                </div>
                <div class="fs-info">
                    <div class="fs-address">
                        Location:
                        <?php echo $locations['address'];?>,
                        <?php echo $locations['postal-code'];?>
                        <?php echo $locations['city'];?>
                    </div>
                    <div id="fs-map"></div>
                    <div class="fs-tag fs-flex">
                        <div>
                            <?php echo $prices['main'];?> <span>NOK</span>
                        </div>
                        <button class="fs-active">send melding</button>
                    </div>
                </div>
            </div>
            <div class="fs-ads fs-flex">
                <?php if ( !empty($medias) ) {?>
                    <div id="fs-carousel">
                        <div onclick="fsCarousel('prev');" class="fs-goLeft"></div>
                        <div onclick="fsCarousel('next');" class="fs-goRight"></div>
                        <div class="fs-pics-count"><?php echo count($medias);?>+</div>
                        <div class="fs-like-price d-flex">
                            <div class="fs-like"
                                 id="fs-like-<?php echo $adId;?>"
                                 data-like="<?php echo $adId;?>"
                                 data-name="<?php echo $searchTitle;?>"
                                 data-address="<?php echo $locations['address'];?>"
                                 data-bra="<?php echo $datas['sizes']['primary'];?>"
                                 data-image="<?php echo $medias[0]['url'];?>"
                                 data-price="<?php echo $prices['main'];?>">
                            </div>
                        </div>
                        <div id="fs-carousel-container">
                            <div id="fs-carousel-wrapper">
                                <ul>
                                    <?php $nImg = 0;
                                    foreach( $medias as $img ) { $nImg++; ?>
                                    <li>
                                        <img src="<?php echo FINN_SEARCH_URL;?>/thumb.php?src=<?php echo $img['url'];?>&w=770&h=497" alt="<?php echo $img['description'];?>">
                                    </li>
                                    <?php } unset($nImg); ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="fs-ad-content">
                    <?php if ($utleid) {?>
                        <div class="utleid">Utleid</div>
                    <?php }?>

                    <div class="fs-desc">
                        <h2>Fasiliteter</h2>
                        <p><?php echo substr($datas['facilities'], 0, -2);?></p>
                        <h2>Beskrivelse</h2>
                        <p><?php echo $datas['ad_value'];?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php if ( !empty($geo) ) { ?>
    <script>
        function initMap() {
            var map = new google.maps.Map(document.getElementById('fs-map'), {
                zoom: 13,
                center: {lat: <?php echo $geo[0];?>, lng: <?php echo $geo[1];?>},
            });

            //var image = '<?php echo FINN_SEARCH_URL;?>/assets/img/map-small.png';
            var beachMarker = new google.maps.Marker({
                position: {lat: <?php echo $geo[0];?>, lng: <?php echo $geo[1];?>},
                map: map,
                animation: google.maps.Animation.DROP,
                //icon: image
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt0c2s4BX0xpYDO2pEEnk-pRpVVGvbOGs&callback=initMap"></script>
<?php } ?>

<?php } ?>
