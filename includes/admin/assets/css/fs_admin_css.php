<?php
/*** set the content type header ***/
header("Content-type: text/css");

/** set the paragraph color ***/
$cTabsBavBackgroundColor = '#b3b3b3';

/*** set the heading size ***/
$heading_size = '2em';

/*** set the heading color ***/
$heading_color = '#c0c0c0';

include "fs_admin_css.css";
?>

.c-tabs-nav__link{
    background-color:<?php echo $cTabsBavBackgroundColor;?>;
}