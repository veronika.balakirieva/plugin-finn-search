var $ = jQuery.noConflict();

$(document).ready(function(){
    $('#fs_plugin_save_button').on('click', function(){
        var filters = [];
        $('.filter').each(function () {
           if( $(this).val() != '' && $(this).prop("checked") ){
               filters.push($(this).val());
           }
        });
        var data = {
            action: 'save_settings',
            api_key: $('#api_key').val(),
            orgId: $('#orgId').val(),
            userAgent: $('#userAgent').val(),
            adUrl: $('#adUrl').val(),
            borderColor: $('#borderColor').val(),
            linkColor: $('#linkColor').val(),
            textColor: $('#textColor').val(),
            priceColor: $('#priceColor').val(),
            filter: filters
        };
        jQuery.post( ajaxurl, data, function(response) {
            if( parseInt(response)===1 ) alert('Saved!');
            if( parseInt(response)===0 ) alert('Something is wrong!');
        });
    });
});