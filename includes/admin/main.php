<?php $options = get_option('finn_search_options');
if (empty($options['filter'])) $options['filter'] = [];
$orgId = fs_get_option('orgId');
if ( !empty($orgId) ) {
  $apiUrl = "https://cache.api.finn.no/iad/search/realestate-letting?orgId=$orgId";
  $fs = new Finn_Search;
  $ps = $fs::fsCurl($apiUrl);
  $filtersArray = $fs::fs_filters($ps, 'admin');  
}
?>

<div class="wrap">
    <div id="tabs" class="c-tabs no-js fs-api-settings">
        <div class="c-tabs-nav">
            <a href="#" class="c-tabs-nav__link is-active">
                <i class="fa fa-home"></i>
                <span>Main</span>
            </a>
            <a href="#" class="c-tabs-nav__link">
                <i class="fa fa-cog"></i>
                <span>API Settings</span>
            </a>
            <a href="#" class="c-tabs-nav__link">
                <i class="fa fa-list-ul"></i>
                <span>API filters</span>
            </a>
            <a href="#" class="c-tabs-nav__link">
                <i class="fa fa-paint-brush"></i>
                <span>Style</span>
            </a>
        </div>
        <div class="c-tab is-active">
            <div class="c-tab__content">
                <h2>Welcome API!</h2>
                <p>
                    The search resources allows you to search for classified ads much similar
                    to <a href="www.finn.no" target="_blank" rel="nofollow">www.finn.no</a>
                </p>
                <p>
                    Use shortcode <strong>[finn-search]</strong> in content editor
                    or <strong>do_shortcode('[finn-search]')</strong> in category template.</p>
                <p>
                    Use shortcode <strong>[finn-bolig]</strong> in content editor
                    or <strong>do_shortcode('[finn-bolig]')</strong> in ad template.
                </p>
            </div>
        </div>
        <div class="c-tab">
            <div class="c-tab__content">
                <h2>API Settings</h2>
                <table class="form-table">
                    <tr>
                        <th scope="row">
                            <label for="api_key">API key</label>
                        </th>
                        <td>
                            <input type="text" id="api_key" name="finn_search_options[api_key]" value="<?php echo $options['api_key']; ?>" class="regular-text">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="orgId">Your organization id</label>
                        </th>
                        <td>
                            <input type="text" id="orgId" name="finn_search_options[orgId]" value="<?php echo $options['orgId']; ?>" class="regular-text">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="userAgent">Your user agent</label>
                        </th>
                        <td>
                            <input type="text" id="userAgent" name="finn_search_options[userAgent]" value="<?php echo $options['userAgent']; ?>" class="regular-text">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="adUrl">Ad url</label>
                        </th>
                        <td>
                            <input type="text" id="adUrl" name="finn_search_options[adUrl]" value="<?php echo $options['adUrl']; ?>" class="regular-text">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="c-tab">
            <div class="c-tab__content">
                <h2>Filters to show</h2>
                <?php if ( !empty($filtersArray) ){ $num=0;?>
                <ul>
                    <li>
                        <input value="sorted-by"
                               type="checkbox"
                               name="finn_search_filters[sorted-by]"
                               class="fs-checkbox filter"
                               id="sorted-by"
                            <?php echo (in_array('sorted-by', $options['filter'])) ? ' checked' : ''; ?>>
                        <label for="sorted-by">Sorted by</label>
                    </li>
                    <li>
                        <input value="search-field"
                               type="checkbox"
                               name="finn_search_filters[search-field]"
                               class="fs-checkbox filter"
                               id="search-field"
                            <?php echo (in_array('search-field', $options['filter'])) ? ' checked' : ''; ?>>
                        <label for="search-field">Search field</label>
                    </li>
                    <li>
                        <input value="price-range"
                        type="checkbox"
                        name="finn_search_filters[price-range]"
                        class="fs-checkbox filter"
                        id="price-range"
                        <?php echo (in_array('price-range', $options['filter'])) ? ' checked' : ''; ?>>
                        <label for="price-range">Price range</label>
                    </li>
                    <?php foreach( $filtersArray as $filter_l_1 ){ $num++;?>
                    <li>
                        <input value="<?php echo $filter_l_1['filterTitle']; ?>"
                               type="checkbox"
                               name="finn_search_filters[<?php echo $filter_l_1['filterTitle'];?>]"
                               class="fs-checkbox filter"
                               id="filter-<?=$num;?>"
                               <?php echo (in_array($filter_l_1['filterTitle'], $options['filter'])) ? " checked" : ''; ?>
                        />
                        <label for="filter-<?=$num;?>"><?php echo $filter_l_1['filterTitle'];?></label>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
            </div>
        </div>
        <div class="c-tab">
            <div class="c-tab__content">
                <h2>Style</h2>
                <table class="form-table">
                    <tr>
                        <th scope="row">
                            <label for="borderColor">Border color</label>
                        </th>
                        <td>
                            <input type="text" id="borderColor" name="finn_search_options[borderColor]" value="<?php echo (!empty($options['borderColor'])) ? $options['borderColor'] : '#e8eef2'; ?>" class="wp-color-result jscolor {hash:true, uppercase:false}">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="textColor">Text color</label>
                        </th>
                        <td>
                            <input type="text" id="textColor" name="finn_search_options[textColor]" value="<?php echo (!empty($options['textColor'])) ? $options['textColor'] : '#2c3a4a'; ?>" class="wp-color-result jscolor {hash:true, uppercase:false}">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="linkColor">Link color</label>
                        </th>
                        <td>
                            <input type="text" id="linkColor" name="finn_search_options[linkColor]" value="<?php echo (!empty($options['linkColor'])) ? $options['linkColor'] : '#2c3a4a'; ?>" class="wp-color-result jscolor {hash:true, uppercase:false}">
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="priceColor">Price color</label>
                        </th>
                        <td>
                            <input type="text" id="priceColor" name="finn_search_options[priceColor]" value="<?php echo (!empty($options['priceColor'])) ? $options['priceColor'] : '#3dafff'; ?>" class="wp-color-result jscolor {hash:true, uppercase:false}">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <button id="fs_plugin_save_button" class="button button-primary">Save settings</button>
</div>

<script>
    var myTabs = tabs({
        el: '#tabs',
        tabNavigationLinks: '.c-tabs-nav__link',
        tabContentContainers: '.c-tab'
    });

    myTabs.init();
</script>
