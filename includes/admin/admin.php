<?php
function fs_add_admin_menu_link(){
    add_menu_page(
        'Finn Search',
        'Finn Search',
        'manage_options',
        '/finn-search/includes/admin/main.php'
    );
    add_submenu_page(
        '/finn-search/includes/admin/main.php',
        'API Search Settings',
        'API',
        'manage_options',
        'api_settings',
        'finn_search_admin_page_screen'
    );

    wp_enqueue_script( 'fs_admin_js', plugin_dir_url( __FILE__ ) . 'assets/js/fs_admin_js.js', array('jquery'));
    wp_enqueue_script( 'fs_tabs_js', plugin_dir_url( __FILE__ ) . 'assets/js/fs_tabs.js', array('jquery'));
    wp_enqueue_script( 'jscolor_js.js', plugin_dir_url( __FILE__ ) . 'assets/js/jscolor.js', array('jquery'));
    wp_enqueue_style( 'fs_admin_css',plugin_dir_url( __FILE__ ) . 'assets/css/fs_admin_css.php');
    wp_enqueue_style( 'font-awesome.min',plugin_dir_url( __FILE__ ) . 'assets/css/font-awesome.min.css');
}
add_action( 'admin_menu', 'fs_add_admin_menu_link' );

add_action('wp_ajax_save_settings', 'finn_search_admin_save_api_settings');
function finn_search_admin_save_api_settings() {
    $value = array(
            'api_key' => sanitize_text_field($_POST['api_key']),
            'orgId' => sanitize_text_field($_POST['orgId']),
            'userAgent' => sanitize_text_field($_POST['userAgent']),
            'adUrl' => sanitize_text_field($_POST['adUrl']),

            'borderColor' => sanitize_text_field($_POST['borderColor']),
            'linkColor' => sanitize_text_field($_POST['linkColor']),
            'textColor' => sanitize_text_field($_POST['textColor']),
            'priceColor' => sanitize_text_field($_POST['priceColor']),

            'filter' => $_POST['filter'],
    );
    $up = update_option( 'finn_search_options', $value , false);
    echo ($up) ? 1 : 0;
    wp_die();
}

function finn_admin_page_states( $state, $post) {
    if ( 'page' != $post->post_type ) {
        return $state;
    }
    $pattern = '/\[(finn[\w\-\_]+).+\]/';
    preg_match_all ( $pattern , $post->post_content, $matches);
    $matches = array_unique( $matches[0] );
    if ( !empty( $matches ) ) {
        $page      = '';
        $shortcode = $matches[0];
        if ( '[finn-search]' == $shortcode ) {
            $page = 'Finn Search Page';
        } elseif ( '[finn-bolig]' == $shortcode ) {
            $page = 'Finn One Ad Page';
        } elseif ( '[finn-wishlist]' == $shortcode ) {
            $page = 'Finn My Wishlist';
        }

        if ( ! empty( $page )) {
            $state['finn'] = $page;
        }
    }
    return $state;
}
add_filter( 'display_post_states', 'finn_admin_page_states', 10, 2 );


/* register menu item */
function finn_search_admin_menu_setup() {
    add_submenu_page(
        'options-general.php',
        'Finn Search Settings',
        'Finn Search',
        'manage_options',
        'finn_search',
        'finn_search_admin_page_screen'
    );
    add_submenu_page(
        'finn-search/includes/boligsok.php',
        'Bolig',
        'Bolig',
        'manage_options',
        'finn-search/includes/bolig.php'
        //'finn_search_admin_page_bolig'
    );
}
//menu setup
//add_action('admin_menu', 'finn_search_admin_menu_setup');

/* display page content */
function finn_search_admin_page_screen() {
    global $submenu;

    // access page settings
    $page_data = array();

    foreach ($submenu['options-general.php'] as $i => $menu_item) {
        if ($submenu['options-general.php'][$i][2] == 'finn_search') {
            $page_data = $submenu['options-general.php'][$i];
        }
    }
    // output
    ?>
    <div class="wrap">
        <?php screen_icon(); ?>
        <h2><?php echo $page_data[3]; ?></h2>
        <form id="msp_helloworld_options" action="options.php" method="post">
            <?php
            settings_fields('finn_search_options');
            do_settings_sections('finn_search');
            submit_button('Save options', 'primary', 'finn_search_options_submit');
            ?>
        </form>
    </div>
    <?php
}

/* settings link in plugin management screen */

function finn_search_settings_link($actions, $file) {
    if (false !== strpos($file, 'finn-search')) {
        $actions['settings'] = '<a href="options-general.php?page=finn_search">Settings</a>';
    }

    return $actions;
}
add_filter('plugin_action_links', 'finn_search_settings_link', 2, 2);


/* settings */
function finn_search_settings_init() {
    register_setting(
        'finn_search_options',
        'finn_search_options',
        'finn_search_options_validate'
    );

    add_settings_section(
        'finn_search_authorbox',
        'API box',
        'finn_search_authorbox_desc',
        'finn_search'
    );

    add_settings_field(
        'finn_search_api_key',
        'API-key',
        'finn_search_api_key_cell',
        'finn_search',
        'finn_search_authorbox'
    );

    add_settings_field(
        'finn_search_orgId',
        'Your organization id',
        'finn_search_orgId_cell',
        'finn_search',
        'finn_search_authorbox'
    );

    add_settings_field(
        'finn_search_userAgent',
        'Your user agent',
        'finn_search_userAgent_cell',
        'finn_search',
        'finn_search_authorbox'
    );

    add_settings_field(
        'finn_search_ad_url',
        'Ad url',
        'finn_search_ad_url',
        'finn_search',
        'finn_search_authorbox'
    );
}
add_action('admin_init', 'finn_search_settings_init');



function finn_search_options_validate($input) {
    global $allowedposttags, $allowedrichhtml;

    if (isset($input['api_key'])) {
        $input['api_key'] = wp_kses_post($input['api_key']);
    }

    return $input;
}


function finn_search_authorbox_desc() {
    echo "<p>The search resources allows you to search for classified ads much similar to www.finn.no <br>
    Use shortcode <strong>[finn-search]</strong> in content editor or <strong>do_shortcode('[finn-search]')</strong> in category template. <br>
    Use shortcode <strong>[finn-bolig]</strong> in content editor or <strong>do_shortcode('[finn-bolig]')</strong> in ad template.</p>";
}


function finn_search_api_key_cell() {
    $options = get_option('finn_search_options');

    $api_key = (isset($options['api_key'])) ? $options['api_key'] : '';
    $api_key = esc_textarea($api_key);
    ?>
    <input type="text" id="api_key" name="finn_search_options[api_key]" value="<?php echo $api_key; ?>">
    <?php
}
function finn_search_orgId_cell() {
    $options = get_option('finn_search_options');

    $orgId = (isset($options['orgId'])) ? $options['orgId'] : '';
    $orgId = esc_textarea($orgId);
    ?>
    <input type="text" id="orgId" name="finn_search_options[orgId]" value="<?php echo $orgId; ?>">
    <?php
}
function finn_search_userAgent_cell() {
    $options = get_option('finn_search_options');

    $userAgent = (isset($options['userAgent'])) ? $options['userAgent'] : '';
    $userAgent = esc_textarea($userAgent);
    ?>
    <input type="text" id="userAgent" name="finn_search_options[userAgent]" value="<?php echo $userAgent; ?>">
    <?php
}
function finn_search_ad_url() {
    $options = get_option('finn_search_options');

    $userAgent = (isset($options['adUrl'])) ? $options['adUrl'] : '';
    $userAgent = esc_textarea($userAgent);
    ?>
    <input type="text" id="adUrl" name="finn_search_options[adUrl]" value="<?php echo $userAgent; ?>">
    <?php
}
?>
