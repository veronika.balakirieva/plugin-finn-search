<?php
$orgId = fs_get_option('orgId');

$adsOnPage = $_REQUEST['rows'];

if( !isset($adsOnPage) ) {$rows = '&rows=14'; $adsOnPage=14;};
!empty($adsOnPage) ? $rows = '&rows='.$adsOnPage : '&rows=14';

if ( isset($_REQUEST['sok']) && !empty($_REQUEST['sok']) ) {
    $q = '&q='.$_REQUEST['sok'];
    $sok = str_replace('-', ' ', $_REQUEST['sok']);
} else {
    $q='';
}

$current_page = $_REQUEST['self'];
!empty($current_page) ? $current_page = '&page='.$current_page : '';

!empty($_REQUEST['price_from']) ? $price_from = $_REQUEST['price_from'] : '';
!empty($_REQUEST['price_to']) ? $price_to = $_REQUEST['price_to'] : '';

!empty($_REQUEST['sort']) ? $sort = $_REQUEST['sort'] : '';

$result = parse_url($_SERVER['REQUEST_URI']);
$par = parse_str($result['query'],$params);

foreach( $params as $key => $param ){
    $paramValues = explode(',',$param);
    $p = 0;
    foreach( $paramValues as $paramValue ){
        $classParam[] = $key.$paramValue;
        $filters .= '&'.$key."=".$paramValue;
    }
}

$apiUrl = "https://cache.api.finn.no/iad/search/realestate-letting?orgId=$orgId".$rows.$q.$filters.$current_page;

$fs = new Finn_Search;
$ps = $fs::fsCurl($apiUrl);
$filtersArray = $fs::fs_filters($ps);
$ads = $fs::fs_ads($ps);
$fs_pagination = $fs::fs_pagination($ps);
$searchTotalResults = $fs::fs_searchTotalResults($ps);
$adUrl = $fs::fs_adUrl();

$option = get_option('finn_search_options');
if (empty($option['filter'])) $option['filter'] = [];

if ( isset($_COOKIE["fslike"]) )
    $fsLiked = explode('?',htmlspecialchars($_COOKIE["fslike"]));
?>

<div class="fs-wrap">
    <div class="fs-header">
        <div class="fs-h1">Find the best deals</div>
        <div class="fs-sub-title">From cozy country houses to stylish city apartments</div>
    </div>
    <form action="<?php echo FINN_SEARCH_URL;?>/includes/boligsok-action.php" method="get" id="fs_itemFilterForm">
        <input type="hidden" name="rows" value="<?php echo $adsOnPage;?>">
        <input type="hidden" name="self" value="<?php echo $page_self;?>">
        <input type="hidden" name="searchTotalResults" value="<?php echo $searchTotalResults;?>">
        <input type="hidden" name="fs-page" value="<?php echo get_page_link(); ?>">
        <div class="fs-content fs-flex fs-top">
            <div class="fs-search-returned">Your search returned <span><?php echo $searchTotalResults; ?></span> results</div>
            <div class="fs-sort fs-ads">
                <?php if( in_array('sorted-by', $option['filter']) ) {?>
                <div class="fs-display-by">
                    <div>Sorted by:</div>
                    <div>
                        <select name="sort" class=" fs-select" onchange="this.form.submit()">
                            <option value="0">Mest relevant</option>
                            <option value="1" <?php echo ($sort==1) ? 'selected' : ''; ?>>Publisert</option>
                            <option value="2" <?php echo ($sort==2) ? 'selected' : ''; ?>>Leie lav-høy</option>
                            <option value="3" <?php echo ($sort==3) ? 'selected' : ''; ?>>Leie høy-lav</option>
                            <option value="4" <?php echo ($sort==4) ? 'selected' : ''; ?>>Areal lav-høy</option>
                            <option value="5" <?php echo ($sort==5) ? 'selected' : ''; ?>>Areal høy-lav</option>
                            <option value="99" <?php echo ($sort==99) ? 'selected' : ''; ?>>Nærmest</option>
                        </select>
                    </div>
                </div>
                <?php }?>
                <div class="fs-display-by">
                    <div>Display by:</div>
                    <div class="fs-rows">
                        <button name="rows" value="6" class="<?php echo $adsOnPage==6?' fs-active':'';?>">6</button>
                        <button name="rows" value="10" class="<?php echo $adsOnPage==10?' fs-active':'';?>">10</button>
                        <button name="rows" value="14" class="<?php echo $adsOnPage==14?' fs-active"':'';?>">14</button>
                    </div>
                </div>
            </div>
            <button class="sFilters fs-active">FILTERS</button>
        </div>
        <div class="fs-content fs-flex">
            <div class="fs-filters fs-filters-catalog">
                <?php if ( in_array('search-field', $option['filter']) ) {?>
                <div class="fs-search">
                    <input id="fs-find" type="text" name="fs_find" value="<?php echo $sok;?>" placeholder="Search">
                    <button class="fs-button-search"></button>
                </div>
                <?php }?>
                <?php if ( in_array('price-range', $option['filter']) ) {?>
                <div class="fs-filter-one">
                    <div class="fs-filter-name">Price</div>
                    <div class="fs-list-filters">
                        <div class="fs-prices">
                            <div class="fs-price">
                                <input type="text" name="price_from" value="<?php echo $price_from;?>" onkeypress="fs_validate(event)" placeholder="From">
                            </div>
                            <div class="fs-price">
                                <input type="text" name="price_to" value="<?php echo $price_to;?>" onkeypress="fs_validate(event)" placeholder="To">
                            </div>
                            <div class="fs-price">
                                <button class="fs-active">Apply</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>

                <? if ( !empty($filtersArray) ){ ?>
                    <?php foreach( $filtersArray as $filter_l_1 ){ ?>
                        <div class="fs-filter-one">
                            <div class="fs-filter-name">
                                <?php echo $filter_l_1['filterTitle']; ?>
                            </div>
                            <div class="fs-list-filters">
                                <ul class="fs-list-group">
                                    <?php $num_filter_l_2 = 0;
                                    foreach( $filter_l_1['attributes'] as $filter_l_2 ){
                                        $num_filter_l_2++; ?>
                                        <?php if ( !empty( $filter_l_2['title']) ){ ?>
                                            <li class=" <?php if (!empty($classParam)){$classP = mb_strtolower($filter_l_1['filterName']).$filter_l_2['value'];if(in_array($classP, $classParam)){echo"active";}}?>">
                                                <input value="<?php echo $filter_l_2['value']; ?>"
                                                       type="checkbox"
                                                       name="<?php echo mb_strtolower($filter_l_1['filterName']);?>[]"
                                                       class="fs-checkbox"
                                                       onclick="changeFilter('#itemFilterForm')"
                                                       id="<?php echo mb_strtolower($filter_l_1['filterName']);?>-<?php echo $filter_l_2['value'];?>"
                                                    <?php if (!empty($classParam)){
                                                        $classP = mb_strtolower($filter_l_1['filterName']).$filter_l_2['value'];
                                                        if (in_array($classP, $classParam)) {echo "checked";}
                                                    }?>
                                                />
                                                <label for="<?php echo mb_strtolower($filter_l_1['filterName']);?>-<?php echo $filter_l_2['value'];?>">
                                                    <?php echo $filter_l_2['title']; ?>
                                                    <span> <?php echo $filter_l_2['totalResults'];?></span>
                                                </label>
                                            </li>
                                            <?php if ( !empty( $filter_l_2['region']) ){ ?>
                                                <?php $num_filter_l_3 = 0;
                                                foreach( $filter_l_2['region'] as $filter_l_3 ){
                                                    $num_filter_l_3++; ?>
                                                    <?php if ( !empty($filter_l_3['city']) ){ ?>
                                                        <li class=" <?php if (!empty($classParam)){$classP=mb_strtolower($filter_l_2['filterName']).$filter_l_3['value'];if(in_array($classP,$classParam)){echo "active";}}?>">
                                                            <input value="<?php echo $filter_l_3['value']; ?>"
                                                                   type="checkbox"
                                                                   name="<?php echo mb_strtolower($filter_l_2['filterName']);?>[]"
                                                                   class="fs-checkbox"
                                                                   onclick="changeFilter('#itemFilterForm')"
                                                                   id="<?php echo mb_strtolower($filter_l_2['title']);?>-<?php echo $num_filter_l_3;?>"
                                                                <?php if (!empty($classParam)){
                                                                    $classP = mb_strtolower($filter_l_2['filterName']).$filter_l_3['value'];
                                                                    if (in_array($classP, $classParam)) {echo "checked";}
                                                                }?>
                                                            />
                                                            <label for="<?php echo mb_strtolower($filter_l_2['title']);?>-<?php echo $num_filter_l_3;?>">
                                                                <?php echo $filter_l_3['city']; ?>
                                                                <span> <?php echo $filter_l_3['totalResults'];?></span>
                                                            </label>
                                                        </li>
                                                    <?php } ?>
                                                <?php }?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="more-filters"></div>
                        </div>
                    <?php } ?>
                <?php } ?>
                <div class="fs-filter-buttons">
                    <button class="fs-button-lg" name="reset" value="reset" >
                        <span>Reset filters</span>
                    </button>
                    <button class="fs-button-lg">
                        <span>Apply filters</span>
                    </button>
                </div>
            </div>
            <div class="fs-ads fs-flex">
                <?php if ( !empty($ads) ) { ?>
                    <?php foreach($ads as $ad) : ?>
                        <div class="fs-unit">
                            <div class="fs-unit-thumb">
                                <a href="<?php echo $adUrl;?>?id=<?php echo $ad['id']?>">
                                    <img src="<?php echo FINN_SEARCH_URL;?>/thumb.php?src=<?php echo $ad['image']?>&w=370&h=250" alt="<?php echo $ad['title']?>">
                                </a>
                            </div>
                            <div class="fs-unit-desc">
                                <div class="fs-unit-name">
                                    <a href="<?php echo $adUrl;?>?id=<?php echo $ad['id']?>">
                                        <?php echo $ad['title']?>
                                    </a>
                                </div>
                                <div class="fs-unit-type">
                                    <div>Adresse:</div>
                                    <div><?php echo $ad['address']?></div>
                                </div>
                                <div class="fs-unit-type">
                                    <div>Område:</div>
                                    <div><?php echo $ad['bra']?></div>
                                </div>
                                <div class="fs-like-price">
                                    <div class="fs-unit-price">
                                        <div><?php echo $ad['mainPrice']?></div>
                                        <div>NOK</div>
                                    </div>
                                    <div class="fs-like"
                                         id="fs-like-<?php echo $ad['id']?>"
                                         data-like="<?php echo $ad['id']?>"
                                         data-name="<?php echo $ad['title'];?>"
                                         data-address="<?php echo $ad['address'];?>"
                                         data-bra="<?php echo $ad['bra'];?>"
                                         data-image="<?php echo $ad['image'];?>"
                                         data-price="<?php echo $ad['mainPrice'];?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php unset($ad);
                    endforeach; ?>
                <?php } else { ?>
                    <div class="fs-empty">
                        <div class="">
                            Ingenting funnet på din forespørsel
                        </div>
                    </div>
                <?php } ?>
                <?php echo $fs_pagination; ?>
            </div>
        </div>
    </form>
</div>