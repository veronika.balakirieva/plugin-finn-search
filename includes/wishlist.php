<?php

if ( isset($_COOKIE["fslike"]) ){
    $fsLiked = explode('?',htmlspecialchars($_COOKIE["fslike"]));
}
$fsLiked = explode(',',$fsLiked[0]);
$searchTotalResults = count($fsLiked);

$orgId = fs_get_option('orgId');
$apiUrl = "https://cache.api.finn.no/iad/search/realestate-letting?orgId=$orgId";

$fs = new Finn_Search;
$ps = $fs::fsCurl($apiUrl);
$ads = $fs::fs_wishlidt($ps, $fsLiked);
$adUrl = $fs::fs_adUrl();
?>

<div class="fs-wrap">
    <div class="fs-header">
        <div class="fs-h1">My wishlist</div>
    </div>
    <form action="<?php echo FINN_SEARCH_URL;?>/includes/boligsok-action.php" method="get" id="fs_itemFilterForm">
        <input type="hidden" name="rows" value="<?php echo $adsOnPage;?>">
        <input type="hidden" name="self" value="<?php echo $page_self;?>">
        <input type="hidden" name="searchTotalResults" value="<?php echo $searchTotalResults;?>">
        <input type="hidden" name="fs-page" value="<?php echo get_page_link(); ?>">
        <div class="fs-content fs-flex fs-top">
            <div class="fs-search-returned">There are <span><?php echo $searchTotalResults; ?></span> ads on your wishlist</div>
        </div>
        <div class="fs-content fs-flex">
            <div class="fs-ads fs-wishlist fs-flex">
                <?php if ( !empty($ads) ) { ?>
                    <?php foreach($ads as $ad) : ?>
                        <div class="fs-unit">
                            <div class="fs-unit-thumb">
                                <a href="<?php echo $adUrl;?>?id=<?php echo $ad['id']?>">
                                    <img src="<?php echo FINN_SEARCH_URL;?>/thumb.php?src=<?php echo $ad['image']?>&w=370&h=250" alt="<?php echo $ad['title']?>">
                                </a>
                            </div>
                            <div class="fs-unit-desc">
                                <div class="fs-unit-name">
                                    <a href="<?php echo $adUrl;?>?id=<?php echo $ad['id']?>">
                                        <?php echo $ad['title']?>
                                    </a>
                                </div>
                                <div class="fs-unit-type">
                                    <div>Adresse:</div>
                                    <div><?php echo $ad['address']?></div>
                                </div>
                                <div class="fs-unit-type">
                                    <div>Område:</div>
                                    <div><?php echo $ad['bra']?></div>
                                </div>
                                <div class="fs-like-price">
                                    <div class="fs-unit-price">
                                        <div><?php echo $ad['mainPrice']?></div>
                                        <div>NOK</div>
                                    </div>
                                    <div class="fs-like"
                                         id="fs-like-<?php echo $ad['id']?>"
                                         data-like="<?php echo $ad['id']?>"
                                         data-name="<?php echo $ad['title'];?>"
                                         data-address="<?php echo $ad['address'];?>"
                                         data-bra="<?php echo $ad['bra'];?>"
                                         data-image="<?php echo $ad['image'];?>"
                                         data-price="<?php echo $ad['mainPrice'];?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php unset($ad);
                    endforeach; ?>
                <?php } else { ?>
                    <div class="fs-empty">
                        <div class="">
                            Ingenting funnet på din forespørsel
                        </div>
                    </div>
                <?php } ?>
                <?php echo $fs_pagination; ?>
            </div>
        </div>
    </form>
</div>