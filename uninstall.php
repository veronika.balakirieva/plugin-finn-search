<?php
if( ! defined('WP_UNINSTALL_PLUGIN') ) exit;

function finn_search_delete_plugin() {
    global $wpdb;

    delete_option('finn_search_options');

    $wpdb->query("DELETE FROM wp_posts WHERE post_name = 'finn-search' ");
    $wpdb->query("DELETE FROM wp_posts WHERE post_name = 'finn-bolig' ");
    $wpdb->query("DELETE FROM wp_posts WHERE post_name = 'finn-wishlist' ");
}

finn_search_delete_plugin();